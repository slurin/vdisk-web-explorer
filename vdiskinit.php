<?php
include_once 'Vdisk.php';
include_once 'config.php';
$oauth2=new \Vdisk\OAuth2(VDISK_CLIENT_ID, VDISK_CLIENT_SECRET);
$tokenObj=loadToken();
if($tokenObj!=NULL and isset($tokenObj->access_token) and $oauth2->getTokenFromObject($tokenObj)){
	$vdiskLogedIn=TRUE;
	$client=new \Vdisk\Client($oauth2, VDISK_AUTH_RIGHTS);
}else{
	$vdiskLogedIn=FALSE;
}