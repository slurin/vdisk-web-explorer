<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="Content-Style-Type" content="text/css">
  <title>Index Of <?php echo $path;?></title>
</head>
<body>

<?php if ($metaData) { ?>

<div><h2>Index Of: <?php echo $metaData->path?></h2></div>

<?php
if (isset($metaData->contents)) {
?>
<div>
	<a href="options.php?command=list&amp;path=<?php echo urlencode(strtr(dirname($path),"\\",'/'));?>">Up</a>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<a href="options.php?command=upload&amp;path=<?php echo urlencode($path);?>&amp;ref=<?php echo urlencode($path);?>">Upload file</a>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<a href="options.php?command=mkdir&amp;path=<?php echo urlencode($path);?>&amp;ref=<?php echo urlencode($path);?>">Create dir</a>
</div>
<table>
	<tr><th>Icon</th><th>Name</th><th>Size</th><th>Time</th><th>Options</th></tr>
		<?php foreach($metaData->contents as $value){ ?>
	<tr>
		<td><img src="images/<?php echo $value->icon;?>.png" /></td>
		<td>
			<a href="<?php
				if($value->is_dir){
					echo 'options.php?command=list&amp;path=',urlencode($value->path);
				}else{
					echo 'options.php?command=download&amp;path=',urlencode($value->path);
				}
			?>"><?php echo @end(explode('/', $value->path));?></a>
		</td>
		<td>
			&nbsp;
			<?php
				if (!$value->is_dir) {
					echo $value->size;
				}else{
					echo '[DIR]';
				}
			?>
			&nbsp;
		</td>
		<td>&nbsp;<?php echo date('Y-m-d H:i:s', strtotime($value->modified));?>&nbsp;</td>
		<td>
			&nbsp;
			<a href="options.php?command=delete&amp;path=<?php echo urlencode($value->path);?>&amp;ref=<?php echo urlencode($path);?>">Delete</a>
			<a href="options.php?command=move&amp;path=<?php echo urlencode($value->path);?>&amp;ref=<?php echo urlencode($path);?>">Move/Rename</a>
		</td>
	</tr>
		<?php } ?>
</table>
<?php } } ?>
</body>
</html>