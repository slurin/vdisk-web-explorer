<?php
function reportError($e){
	header('Content-Type: text/plain; charset=utf-8');
	echo $e->getCode(),': ',$e->getMessage();
	exit;
}
session_start();
include_once dirname(__FILE__).'/vdiskinit.php';
if(!isset($_REQUEST['command'])){
	header('Location: index.php');
}
$ref=isset($_REQUEST['ref'])?$_REQUEST['ref']:'/';
switch($_REQUEST['command']){
	case 'list':
		if(!VDISK_PUBLIC_VIEW and !(isset($_SESSION['admin']) and $_SESSION['admin'])){
			header('Location: options.php?command=login');
			exit;
		}else{
			if($vdiskLogedIn){
					try{
						if (isset($_GET['path'])) {
							$path=$_GET['path'];
						}else{
							$path='/';
						}
						$response=$client->metaData($path);
						$metaData=$response['body'];
						include_once APPROOT.'/tpl/list.php';
					}catch(\Vdisk\Exception $e){
						reportError($e);
					}
			}else{
				header('Location: options.php?command=auth');
				exit;
			}
		}
		break;
	case 'download':
		if(!VDISK_PUBLIC_DOWNLOAD and !(isset($_SESSION['admin']) and $_SESSION['admin'])){
			header('Location: options.php?command=login');
			exit;
		}else{
			if($vdiskLogedIn){
				try{
					if(isset($_GET['path'])){
						$path = $_GET['path'];
					}else{
						$path = '/';
					}
					$response=$client->media($path);
					header('Location: '.$response['body']->url);
				}catch(\Vdisk\Exception $e){
					reportError($e);
				}
			}else{
				header('Location: options.php?command=auth');
			}
		}
		break;
	case 'auth':
		if(isset($_SESSION['admin']) and $_SESSION['admin']){
			if(!$vdiskLogedIn){
				header('Location: '.$oauth2->getAuthorizeURL(VDISK_CALLBACK_URL, 'code', time(), 'mobile'));
			}else{
				echo 'Authed.';
			}
		}else{
			header('Location: options.php?command=login');
		}
		break;
	case 'mkdir':
		if(isset($_SESSION['admin']) and $_SESSION['admin']){
			if($vdiskLogedIn){
				if(isset($_REQUEST['path'])){
					$path = $_REQUEST['path'];
				}else{
					$path = '/';
				}
				if(isset($_REQUEST['dirname']) and $_REQUEST['dirname']!=''){
					try{
						$client->create($path.'/'.$_REQUEST['dirname']);
						header('Location: options.php?command=list&path='.$path);
					}catch(\Vdisk\Exception $e){
						reportError($e);
					}
				}else{
					include_once APPROOT.'/tpl/mkdir.php';
				}
			}else{
				header('Location: options.php?command=auth');
			}
		}else{
			header('Location: options.php?command=login&ref='.urlencode($path));
		}
		break;
	case 'delete':
		if(isset($_SESSION['admin']) and $_SESSION['admin']){
			if($vdiskLogedIn){
				if(isset($_REQUEST['path']) and $_REQUEST['path']!='/'){
					$path = $_REQUEST['path'];
					try{
						$client->delete($path);
						header('Location: options.php?command=list&path='.urlencode($ref));
					}catch(\Vdisk\Exception $e){
						reportError($e);
					}
				}else{
					echo 'Delete file/dir path cannot be empty or \'/\'';
				}
			}else{
				header('Location: options.php?command=auth');
			}
		}else{
			header('Location: options.php?command=login&ref='.urlencode($ref));
		}
		break;
	case 'move':
		if(isset($_SESSION['admin']) and $_SESSION['admin']){
			if($vdiskLogedIn){
				if(isset($_REQUEST['path']) and $_REQUEST['path']!='/'){
					$path=$_REQUEST['path'];
					if(isset($_REQUEST['topath'])){
						$topath=strtr($_REQUEST['topath'],"\\\"'^<>?*",'________');
						try{
							if(isset($_REQUEST['copymode'])){
								$client->copy($path,$topath);
							}else{
								$client->move($path,$topath);
							}
							header('Location: options.php?command=list&path='.$ref);
						}catch(\Vdisk\Exception $e){
							reportError($e);
						}
					}else{
						include_once APPROOT.'/tpl/move.php';
					}
				}else{
					echo 'Move file/dir path cannot be empty or \'/\'';
				}
			}else{
				header('Location: options.php?command=auth');
			}
		}else{
			header('Location: options.php?command=login&ref='.urlencode($ref));
		}
		break;
	case 'upload':
		if(isset($_SESSION['admin']) and $_SESSION['admin']){
			if($vdiskLogedIn){
				if(isset($_REQUEST['path'])){
					$path = $_REQUEST['path'];
				}else{
					$path = '/';
				}
				if(isset($_FILES['uploadFile']['name'])){
					if(!$_FILES['uploadFile']['error'] and $_FILES['uploadFile']['name'] and $_FILES['uploadFile']['size']>0){
						@ini_set('max_execution_time', '1800');
						@set_time_limit(1800);
						try{
							$tempFile=APPROOT.'/cache/'.md5(mt_rand(0,99999999).time());
							move_uploaded_file($_FILES['uploadFile']['tmp_name'],$tempFile);
							$fp=fopen($tempFile,'rb');
							$client->putStream($fp,$path.'/'.$_FILES['uploadFile']['name']);
							fclose($fp);
							header('Location: options.php?command=list&path='.urlencode($path));
							unlink($tempFile);
						}catch(\Vdisk\Exception $e){
							reportError($e);
						}
					}else{
						echo 'Not a valid file selected.';
					}
				}else{
					include_once APPROOT.'/tpl/upload.php';
				}
			}else{
				header('Location: options.php?command=auth');
			}
		}else{
			header('Location: options.php?command=login&ref='.urlencode($path));
		}
		break;
	case 'login':
		if(isset($_REQUEST['psw']) and $_REQUEST['psw']==VDISK_APP_ADMINPSW){
			$_SESSION['admin']=TRUE;
			header('Location: options.php?command=list&path='.urlencode($ref));
		}else{
			include_once APPROOT.'/tpl/login.php';
		}
		break;
	case 'logout':
		session_destroy();
		header('Location: options.php?command=list');
		break;
	default:
		header('Location: index.php');
		break;
}